package com.nalxsdevelop.recyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.graphics.Color.*;

public class secondActivity extends AppCompatActivity {
ImageView imageViewActivity;
TextView textViewActivity;
    TextView modelo,placas,nameCaja,modeloCaja,placasCaja,eco,ecoCaja,nameOperador,cel,status;
String name;
    String modelo1;
    String nameCaja1;
    String modeloCaja1;
    String status1;
    String placas1;
    String placasCaja1;
    String operador1;
    String numeroCel1;
    String ecoCar1;
    String ecoCaja1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        imageViewActivity = findViewById(R.id.imageViewSecondActivity);
        textViewActivity = findViewById(R.id.nameSecondActivity);
        modelo = findViewById(R.id.txtModeloSecond);
        placas = findViewById(R.id.txtPlaca);
        nameCaja= findViewById(R.id.textCajaNameSecond);
        modeloCaja = findViewById(R.id.textModeloSecondCaja);
        placasCaja = findViewById(R.id.textPlacasSecondCaja);
        ecoCaja = findViewById(R.id.textEcoCaja);
        eco = findViewById(R.id.textEcoCar);
        nameOperador = findViewById(R.id.textnameOperador);
        cel = findViewById(R.id.textNumeroOperador);
        status = findViewById(R.id.texStatusSecond);


        obtenerNombres();
        setNombres();
    }

    private void obtenerNombres(){
            if (getIntent().hasExtra("nombres")){

                name = getIntent().getStringExtra("nombres");
                modelo1 = getIntent().getStringExtra("modelo");
                nameCaja1 = getIntent().getStringExtra("namecaja");
                modeloCaja1 = getIntent().getStringExtra("modelocaja");
                status1 = getIntent().getStringExtra("status");
                placas1 = getIntent().getStringExtra("placas");
                placasCaja1 = getIntent().getStringExtra("placasCaja");
                operador1 = getIntent().getStringExtra("operador");
                numeroCel1 = getIntent().getStringExtra("numero");
                ecoCar1 = getIntent().getStringExtra("eco");
                ecoCaja1 = getIntent().getStringExtra("ecoCaja");


            }else {
                Toast.makeText(this,"No hay informacion",Toast.LENGTH_LONG).show();
            }

    }

    private void setNombres(){
        textViewActivity.setText(name);
        modelo.setText(modelo1);
        placas.setText(placas1);
        nameCaja.setText(nameCaja1);
        modeloCaja.setText(modeloCaja1);
        placasCaja.setText(placasCaja1);
        eco.setText(ecoCar1);
        ecoCaja.setText(ecoCaja1);
        nameOperador.setText(operador1);
        cel.setText(numeroCel1);

        status.setText(status1);








    }
}
