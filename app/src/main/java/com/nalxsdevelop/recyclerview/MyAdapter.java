package com.nalxsdevelop.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    String names[];
    String modelo[];
    String nameCaja[];
    String modeloCaja[];
    String status[];
    String placas[];
    String placasCaja[];
    String operador[];
    String numeroCel[];
    String ecoCar[];
    String ecoCaja[];

    Context context;

    public MyAdapter(Context context,   String names[],String modelo[],String nameCaja[],
                     String modeloCaja[], String status[],String placas[],String placasCaja[],String operador[],
                     String numeroCel[],String ecoCar[], String ecoCaja[]){

        this.context = context;
        this.names= names;
        this.modelo = modelo;
        this.nameCaja= nameCaja;
        this.modeloCaja = modeloCaja;
        this.status = status;
        this.placas = placas;
        this.placasCaja = placasCaja;
        this.operador = operador;
        this.numeroCel = numeroCel;
        this.ecoCar= ecoCar;
        this.ecoCaja =ecoCaja;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater= LayoutInflater.from(context);

        View view =inflater.inflate(R.layout.my_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tex1.setText(names[position]);
        holder.modelCar.setText(modelo[position]);
        holder.nameCaja.setText(nameCaja[position]);
        holder.modelCaja.setText(modeloCaja[position]);


        if (status[position]=="Disponible"){
            holder.estatusCar.setTextColor(Color.rgb(0,143,57));
            holder.estatusCar.setText(status[position]);
        }else{
            holder.estatusCar.setTextColor(Color.RED);
            holder.estatusCar.setText(status[position]);
        }


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,secondActivity.class);
                intent.putExtra("nombres",names[position]);
                intent.putExtra("modelo",modelo[position]);
                intent.putExtra("namecaja",nameCaja[position]);
                intent.putExtra("modelocaja",modeloCaja[position]);
                intent.putExtra("status",status[position]);
                intent.putExtra("placas",placas[position]);
                intent.putExtra("placasCaja",placasCaja[position]);
                intent.putExtra("operador",operador[position]);
                intent.putExtra("numero",numeroCel[position]);
                intent.putExtra("eco",ecoCar[position]);
                intent.putExtra("ecoCaja",ecoCaja[position]);



                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ConstraintLayout mainLayout;

        TextView tex1;
        TextView modelCar;
        TextView nameCaja;
        TextView modelCaja;
        TextView estatusCar;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            modelCar = itemView.findViewById(R.id.txviewModelo);
            nameCaja= itemView.findViewById(R.id.txtviewCaja);
            modelCaja = itemView.findViewById(R.id.txviewModelCaja);
            estatusCar = itemView.findViewById(R.id.txtViewStatus);

            tex1 = itemView.findViewById(R.id.textView123);


            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}
