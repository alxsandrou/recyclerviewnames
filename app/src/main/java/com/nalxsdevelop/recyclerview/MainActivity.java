package com.nalxsdevelop.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String names[]= new String[]{"Cascadia Rojo", "Cascadia Azul","Kenworth Blanco", "Peterbilt Blanco","Kenworth Rojo", "Freightliner Blanco","Kenworth Blanco", "Kenworth Dorado","Freightliner Rojo", "International Naranja"};
    String modelo[]= new String[]{"2009 ", "2009","2014", "2012","2008", "2006","2006", "2007","2009", "2002"};
    String nameCaja[]= new String[]{"Caja Utility 48ft", "Caja Great Dane 48ft","Caja Utility 51ft", "Caja Utility 48ft","Caja Great Dane 51ft", "Caja Utility 51ft","Caja Utility 48ft", "Caja Utility 48ft","Caja Utility 48ft", "Caja Utility 51ft"};
    String modelCaja[]= new String[]{"2011", "2012","2008", "2010","2012", "2011","2010", "2010","2011", "2011"};
    String status[]= new String[]{"Disponible", "No Disponible","No Disponible", "No Disponible","Disponible", "Disponible","No Disponible", "Disponible","Disponible", "No Disponible"};
    String placas[]= new String[]{"60AH5W", "60AH5W 60AH5W","60AH5W", "60AH5W","60AH5W", "60AH5W","60AH5W", "60AH5W","60AH5W", "60AH5W"};
    String placasCaja[]= new String[]{"56UG4H", "56UG4H","56UG4H", "56UG4H","56UG4H", "56UG4H","56UG4H", "56UG4H","56UG4H", "56UG4H"};
    String operador[]= new String[]{"Luis Navarro Peréz", "Demetrio Rodríguez Altamirano","Bryan Molina Cruz", "Felipe Castillo Sanchez","Marco Lopez Luna", "Gerardo Ramirez Estevez","Victor Cruz Landa", "Felipe Castillo Sanchez","Demetrio Rodríguez Altamirano", "Luis Navarro Peréz"};
    String numeroCel[]= new String[]{"5596323265", "7784565532","5587456532", "5532659878","5545781245", "7789562321","5565325241", "5598653252","5565325241", "5565325241"};
    String ecoCar[]= new String[]{"ECO 06", "ECO 08","ECO 07", "ECO 05","ECO 09", "ECO 11","ECO 03", "ECO 04","ECO 05", "ECO 12"};
    String ecoCaja[]= new String[]{"ECO 114", "ECO 111","ECO 112", "ECO 113","ECO 110", "ECO 116","ECO 109", "ECO 108","ECO 115", "ECO 107"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerViewMain);

        MyAdapter myAdapter = new MyAdapter(this,names,modelo,nameCaja,modelCaja,status,placas,placasCaja,operador,numeroCel,ecoCar,ecoCaja);

        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
